import os
import sys
import time

import pandas as pd
import pyodbc
import sqlalchemy
from pprint import pprint

print("Mediapipe Start")

start_timestamp = float(sys.argv[1])
end_timestamp = float(sys.argv[2])

ROOT_DIR = os.path.expanduser('~')
desktop_path = ROOT_DIR + "/Desktop"
prevla_sensors = desktop_path + "/prevla/prevla_sensors"
backend_path = desktop_path + "/prevla/prevla_be"
source_path = f"/{backend_path}/data_collection/sensor_data/mediapipe_local.csv"


with open("/home/inosens/Desktop/prevla/prevla_be/mediapipe_data_headers.txt", 'r') as my_file:
    header_names = my_file.read()


mediapipe_data = pd.read_csv(source_path, sep=',',
                             header=None, names=header_names.split(","))
print("mediapipe data has readen from local csv")

# filtering local mediapipe csv by exp timestamps
mediapipe_data = mediapipe_data.loc[(mediapipe_data["mp_1_p1_mp_h_p_timestamp"] >= float(start_timestamp)) & (
    mediapipe_data["mp_1_p1_mp_h_p_timestamp"] <= float(end_timestamp))].reset_index(drop=True)

engine = sqlalchemy.create_engine(
    "mssql+pyodbc://SA:hP337^9nArG&@193.35.200.106/Prevla?driver=ODBC+Driver+18+for+SQL+Server&TrustServerCertificate=Yes",
    connect_args={'trustServerCertificate': 'Yes'})
print("query has completed for mediapipe")

# TO DO : Check to_sql() functionre
response = mediapipe_data.to_sql(
    "MediapipeDatas", engine, if_exists='append', index=False)

print("Mediapipe data has added to database")
