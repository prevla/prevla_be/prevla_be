import json
import pyodbc
from flask import Flask, request, jsonify, render_template, Response
import subprocess
import psutil
from flask_cors import CORS
import os
import time
import pandas as pd
import numpy as np
from datetime import datetime

# DB connection
cnxn = pyodbc.connect(
    'DRIVER={ODBC Driver 18 for SQL Server};SERVER=193.35.200.106;DATABASE=Prevla;UID=SA;PWD=hP337^9nArG&;TrustServerCertificate=Yes')
print(cnxn, "DB connected")

app = Flask(__name__)

CORS(app)

ROOT_DIR = os.path.expanduser('~')
desktop_path = ROOT_DIR + "/Desktop"
prevla_sensors = desktop_path + "/prevla/prevla_sensors"
backend_path = desktop_path + "/prevla/prevla_be"

radarProc = 0
radarStartTimeStamp = 0
mediaPipeProc = 0
raw = False
repeat = 0
sheet_id = "1rgDXUd2MF09pKZw3l5f3DgwmLn_lf0AZuhibcorhaCk"
sheet_name = "conf"
url = f"https://docs.google.com/spreadsheets/d/1rgDXUd2MF09pKZw3l5f3DgwmLn_lf0AZuhibcorhaCk/gviz/tq?tqx=out:csv&sheet=conf"


@app.route("/", methods=['GET'], strict_slashes=False)
def hello():
    return "Hello World", 200


@app.route("/sensor/open/mediapipe", methods=['POST'])
def open_mediapipe():

    try:
        global mediaPipeProc

        mediaPipeProc = subprocess.Popen(
            f"cd {prevla_sensors}/mediapipe  && /home/inosens/miniconda3/envs/mp/bin/python mediapipe_detection.py 1", shell=True)

        return json.dumps({"status": "ok"}), 200

    except Exception as e:
        return str(e), 500


@app.route("/sensor/open/radar", methods=['POST'])
def opRadar():

    try:
        data = request.json
        global radarProc
        global radarStartTimeStamp

        def find_last(yy, conf_no):
            description = ''
            name = ''
            val = yy.iloc[0:150, conf_no]
            df = val.replace(np.nan, '', regex=True)
            for i, item in enumerate(df):
                if i == 1:
                    description = item
                if i == 2:
                    name = item
                if item == '':
                    break
            return i, description, name

        def get_config_from_drive(path, id, sheet_id, sheet_name):
            path = ''
            id = ''
            yy = pd.read_csv(url)
            return yy

        path = ''
        id = ''
        sheet_id = ''
        sheet_name = ''
        yy = get_config_from_drive(path, id, sheet_id, sheet_name)
        shifter = 3
        if (data.get('config')):
            conf_no = shifter+int(int(data.get('config')))

            val = yy.iloc[0:500, conf_no]
            lister = val.values.tolist()
            last = find_last(yy, conf_no)
            lister = lister[3:last[0]]

            with open(f"{prevla_sensors}/mmwave_ti_ros/ros_driver/src/ti_mmwave_rospkg/cfg/1843_3d.cfg", "w") as f:
                for s in lister:
                    f.write(str(s) + "\n")

            print("Configuration is success")
            # config hakkında bilgi basılabilir
            print("description   :: ", last[1])
            print("dosya ismi :: ", last[2])

        # https://docs.google.com/spreadsheets/d/1rgDXUd2MF09pKZw3l5f3DgwmLn_lf0AZuhibcorhaCk/edit?usp=sharing

        print("ben")

        radarProc = subprocess.Popen(
            f"cd && cd {prevla_sensors}/mmwave_ti_ros/ros_driver && source devel/setup.bash && roslaunch ti_mmwave_rospkg 1843_multi_3d_0.launch",
            shell=True,
            executable="/bin/bash")

        radarStartTimeStamp = time.time()

        return json.dumps({"status": "ok"}), 200

    except Exception as e:
        return str(e), 500


@app.route("/sensor/check/mediapipe", methods=['POST'])
def checkMedia():

    return str(bool(mediaPipeProc)), 200


@app.route("/sensor/check/radar", methods=['POST'])
def checkRadar():

    return str(bool(radarProc)), 200


@app.route('/close', methods=['POST'])
def close():

    try:
        procList = []
        procList.append(radarProc)
        procList.append(mediaPipeProc)

        for proc in procList:
            parent = psutil.Process(proc.pid)
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()

        return json.dumps({"status": "ok"}), 200

    except Exception as e:
        return str(e), 500


@app.route('/sensor/close/radar', methods=['POST'])
def closeRadar():

    try:
        global radarProc
        global radarStartTimeStamp

        parent = psutil.Process(radarProc.pid)

        for child in parent.children(recursive=True):
            child.kill()
        parent.kill()

        radarProc = 0
        radarStartTimeStamp = 0

        print("radar has closed")
        return json.dumps({"status": "ok"}), 200

    except Exception as e:
        return str(e), 500


@app.route('/sensor/close/mediapipe', methods=['POST'])
def closeMediapipe():

    try:
        global mediaPipeProc

        parent = psutil.Process(mediaPipeProc.pid)

        for child in parent.children(recursive=True):
            child.kill()
        parent.kill()

        response = {"status": "ok"}
        mediaPipeProc = 0
        return json.dumps(response), 200

    except Exception as e:
        return str(e), 500


@app.route('/raw_start', methods=['POST'])
def raw_start():
    try:

        # This code is retrieving the JSON data from the request object and extracting the values of
        # the 'total_time' and 'repeat' keys from the JSON data. These values are then assigned to the
        # variables 'totalTime' and 'repeat', respectively.
        raw_request = request.get_json()
        totalTime = raw_request['total_time']
        repeat = raw_request['repeat']

        # Second to ms
        totalTime = int(totalTime)*1000

        # Taking last experiment number from db
        cursor = cnxn.cursor()
        query_of_get_last_exp = "SELECT TOP(1) deney_no FROM Data_Logs ORDER BY Id DESC"
        cursor.execute(query_of_get_last_exp)
        last_exp = cursor.fetchone()
        cnxn.commit()

        # raw data name
        name_of_exp = "experiment_" + str(last_exp[0]) + "_" + str(repeat)

        # Config raw data
        filename = f'/{prevla_sensors}/DCA1000EVM_CLI_ROS/SourceCode-20230305T092254Z-001/SourceCode/Release/configFile.json'
        with open(filename, 'r') as f:
            data = json.load(f)
            data['DCA1000Config']['captureConfig']['filePrefix'] = name_of_exp
            data['DCA1000Config']['captureConfig']['durationToCapture_ms'] = totalTime

        os.remove(filename)
        with open(filename, 'w') as f:
            json.dump(data, f, indent=4)

        # Start raw data collection
        subprocess.Popen(
            f"cd {prevla_sensors}/DCA1000EVM_CLI_ROS/SourceCode-20230305T092254Z-001/SourceCode/Release && ./raw_conf.sh ", shell=True)

        return json.dumps({"status": "ok"}), 200

    except Exception as e:
        print(e)
        return str(e), 500


@app.route('/raw_stop', methods=['POST'])
def raw_stop():
    try:
        subprocess.Popen(
            f"cd {prevla_sensors}/DCA1000EVM_CLI_ROS/SourceCode-20230305T092254Z-001/SourceCode/Release && sleep 2 && ./raw_killer.sh && ./raw_stop.sh", shell=True)
        return "OK", 200
    except Exception as e:
        return str(e), 500


@app.route('/save/log', methods=['POST'])
def log():

    try:
        # Taking data
        log_data = request.get_json()

        # Convert data to list
        result = [list(d.values()) for d in log_data]

        # open db cursor
        cursor = cnxn.cursor()

        # takes last exp_ number from db
        query_of_get_last_exp = "SELECT TOP(1) deney_no FROM Data_Logs ORDER BY Id DESC"
        cursor.execute(query_of_get_last_exp)
        last_exp = cursor.fetchone()
        if (last_exp != None):
            last_exp = last_exp[0]
        else:
            last_exp = 0

        # Insert data to db
        log_sql_text = """ INSERT INTO [dbo].[Data_Logs]
                ([deney_no]
                ,[company_name]
                ,[expert_name]
                ,[patient_name]
                ,[current_part]
                ,[total_number_part]
                ,[current_repeat]
                ,[total_number_repeat]
                ,[duration_part]
                ,[total_duration]
                ,[startTimestamp_current_part]
                ,[endTimestamp_current_part]
                ,[date]
                ,[config]
                ,[pose_number]
                ,[description])
            VALUES
                (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) """

        # insert logs to db
        for index in result:
            index.insert(0, last_exp+1)
            # print(type(index[12]))
            index[12] = datetime.strptime(index[12], '%d.%m.%Y %H:%M:%S')
            cursor.execute(log_sql_text, tuple(index))

        # commit db
        cnxn.commit()

        # script that saves the radar data of the experiment to the database
        radar_data_command = "/home/inosens/miniconda3/envs/server/bin/python /home/inosens/Desktop/prevla/prevla_be/radar_data.py " + \
            str(result[0][10]) + " " + str(result[len(result)-1][11])

        subprocess.Popen(radar_data_command,
                         executable="/bin/bash", shell=True)

        # script that saves the mediapipe data of the experiment to the database
        media_data_command = "/home/inosens/miniconda3/envs/server/bin/python /home/inosens/Desktop/prevla/prevla_be/media_data.py " + \
            str(result[0][10]) + " " + str(result[len(result)-1][11])
        subprocess.Popen(media_data_command,
                         executable="/bin/bash", shell=True)

        return "OK", 200

    except Exception as e:
        print(e)
        return str(e), 500


@app.route('/expList', methods=['POST'])
def expList():
    try:
        # open db cursor
        cursor = cnxn.cursor()

        # takes last exp_ number from db
        log_query = "SELECT [Id],[deney_no],[company_name],[expert_name],[patient_name],[total_duration],[date],[pose_number],[description] FROM Data_Logs ORDER BY Id DESC;"
        log_data = pd.read_sql(log_query, cnxn)

        # cursor.execute(query_of_get_last_exp)
        # results = cursor.fetchall()
        # print(results)

        # exp_list = [row[0] for row in results]
        # exp_list = set(exp_list)
        # exp_list = list(exp_list)

        # json_data = json.dumps(exp_list)
        # print(json_data)
        return log_data.to_json(orient="index"), 200

    except Exception as e:
        print(e)
        return str(e), 500


@app.route('/figure', methods=['POST'])
def figure():
    try:
        figure_request = request.get_json()
        experiment = figure_request['exp']
        # shell_command = 'bash -c "conda activate figure && python dataViewerfromDB.py 115"'
        # subprocess.Popen(shell_command, shell=True)
        figure_process = f"cd {desktop_path}/prevla/prevla_viewer && /home/inosens/miniconda3/envs/figure/bin/python dataViewerfromDB.py " + \
            str(experiment)
        subprocess.Popen(figure_process, shell=True)
        # os.system("gnome-terminal -x conda run -n figure python dataViewerfromDB.py 115")
        return "OK", 200

    except Exception as e:
        return str(e), 500


if __name__ == '__main__':
    app.run(port=3750, debug=True)
