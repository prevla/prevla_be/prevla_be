import csv
import os
import sys
import time

import pandas as pd
import pyodbc
import sqlalchemy

start_timestamp = float(sys.argv[1])
end_timestamp = float(sys.argv[2])


ROOT_DIR = os.path.expanduser('~')
desktop_path = ROOT_DIR + "/Desktop"
backend_path = desktop_path + "/prevla/prevla_be"
csv_file_path = f'{backend_path}/data_collection/sensor_data/radar_data_doga.csv'
num_days_timestamp = 604800  # 7 days back


def delete_rows(csv_file_path):
    today = time.time()
    days_to_go_back = today - num_days_timestamp

    with open(csv_file_path, 'r') as file:
        reader = csv.reader(file)
        rows = list(reader)

    new_rows = [row for row in rows if float(row[9]) >= days_to_go_back]

    with open(csv_file_path, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(new_rows)


delete_rows(csv_file_path)

radar_data = pd.read_csv(csv_file_path, sep=',', header=None, names=["rdr_experiment_no", "rdr_frame_no", "rdr_total_points_in_frame",
                                                                     "rdr_point_no_in_frame", "rdr_y_radar", "rdr_x_radar", "rdr_z_radar",
                                                                     "rdr_vel_radar", "rdr_intensity_radar", "rdr_timestamp", "rdr_user",
                                                                     "rdr_date"])

radar_data = radar_data.loc[(radar_data['rdr_timestamp'] >= float(start_timestamp)) &
                            (radar_data['rdr_timestamp'] <= float(end_timestamp))].reset_index(drop=True)

radar_data["rdr_date"] = pd.to_datetime(radar_data["rdr_date"])

engine = sqlalchemy.create_engine(
    "mssql+pyodbc://SA:hP337^9nArG&@193.35.200.106/Prevla?driver=ODBC+Driver+18+for+SQL+Server&TrustServerCertificate=Yes",
    connect_args={'trustServerCertificate': 'Yes'})


radar_data.to_sql("RadarDatas", engine, if_exists='append', index=False)
print("Radar Data has added to databases")
